var typo3cache = {host: location.hostname, url: location.origin + '/typo3/'};
var fetchTypo3Caches = function(callback, settings) {
    var subfolder = '/';
    if (settings.subfolders[typo3cache.host]) {
        subfolder = '/' + settings.subfolders[typo3cache.host];
        if (subfolder.substr(-1, 1) !== '/') {
            subfolder = subfolder + '/';
        }
    }

    $.ajax({ url: location.origin + subfolder + 'typo3/' })
        .done(function( data, status, xhr ) {
            data = data.replace(/<img(\b[^>]*>)/ig, '<lazyimg$1');

            var $cacheMenu = $(data).find('#typo3-cms-backend-backend-toolbaritems-clearcachetoolbaritem > .dropdown-menu');
            if ($cacheMenu.length === 0) {
                // TYPO3 6.2 support
                $cacheMenu = $(data).find('#clear-cache-actions-menu');
                $cacheMenu.find('.toolbar-item-menu').show();
                $cacheMenu.support = '6.2';
            }

            if ($cacheMenu.length === 0) {
                // okay, no cache menu found. is this typo3?
                if ($(data).find('.typo3-login-container').add('#typo3-login-form').length === 0) {
                    // no. required classes not found in source code
                    typo3cache.status = 404;
                    typo3cache.hasTypo3 = false;
                    if (callback) {
                        callback(typo3cache);
                    }
                    return;
                } else {
                    // yes. login required
                    typo3cache.status = 401;
                    typo3cache.hasTypo3 = true;
                }
            } else {
                typo3cache.status = 200;
                typo3cache.hasTypo3 = true;
            }

            $cacheMenu.find('a').each(function(){
                if ($(this).attr('href') === '#') {
                    $(this).remove();
                } else {
                    var link = ($(this).attr('href').substr(0, 1) === '/') ? $(this).attr('href') : '/' + $(this).attr('href');

                    if (link.substr(0,6) !== '/typo3') {
                        link = '/typo3' + link;
                    }

                    var href = location.origin + link;
                    $(this).attr('href', href);
                }
            });

            $cacheMenu.find('lazyimg').each(function(){
                var link = ($(this).attr('src').substr(0, 1) === '/') ? $(this).attr('src') : '/' + $(this).attr('src');
                var src = location.origin + link;
                $(this).attr('src', src);
            });

            typo3cache.content = $cacheMenu.html();
            typo3cache.http_status = xhr.status;
            if (callback) {
                callback(typo3cache);
            }
        })
        .fail(function(xhr) {
            typo3cache.status = 500;
            typo3cache.http_status = xhr.status;
            typo3cache.error = true;
            if (callback) {
                callback(typo3cache);
            }
        });
};

chrome.runtime.onMessage.addListener(function (request, sender, response) {
    if ((request.from === 'popup') && (request.subject === 'DOMInfo')) {
        fetchTypo3Caches(function(cache){
           response(cache);
        }, request.settings);
        return true;
    }
});
