chrome.runtime.onMessage.addListener(function (request, sender) {
	var settings;

	function load_settings() {
		if (localStorage['settings']) {
			settings = $.parseJSON(localStorage['settings'])
			if (!settings.subfolders) {
				settings.subfolders = {}
			}
		}
	}
	load_settings();

	if ((request.from === 'popup') && (request.subject === 'clearcache')) {
		// Set badge text: ...
		chrome.browserAction.setBadgeText({text: '...'});
		chrome.browserAction.setBadgeBackgroundColor({color: '#666'});

		$.ajax({ url: request.url })
			.done(function( data, status, xhr) {
				// Set badge text: OK
				chrome.browserAction.setBadgeText({text: 'OK'});
				chrome.browserAction.setBadgeBackgroundColor({color: '#31DB0F'});

				if (settings.reloadPageAfterCacheClear) {
					chrome.tabs.getSelected(null, function(tab) {
						var code = 'window.location.reload();';
						chrome.tabs.executeScript(tab.id, {code: code});
					});
				}
				// Remove badge text and reload current tab delayed
				setTimeout(function(){
					chrome.browserAction.setBadgeText({text: ''});
				}, 1500);
			}).fail(function( xhr ) {
				// Set badge text: ERR
				chrome.browserAction.setBadgeText({text: 'ERR'});
				chrome.browserAction.setBadgeBackgroundColor({color: '#DB530F'});

				// Remove badge text delayed
				setTimeout(function(){
					chrome.browserAction.setBadgeText({text: ''});
				},1500);
			});
	}

	if ((request.from === 'popup') && (request.subject === 'setbadge')) {
		chrome.browserAction.setBadgeText({text: request.text});
		chrome.browserAction.setBadgeBackgroundColor({color: request.color});
		if (request.sticky !== true) {
			setTimeout(function() {
				chrome.browserAction.setBadgeText({text: ''});
			}, 1500);
		}

	}

	if ((request.from === 'popup') && (request.subject === 'seticon')) {

		var setIcon = function(icon) {
			chrome.browserAction.setIcon({
				path: icon
			});
		};

		if (request.delay) {
			setTimeout(function(){
				setIcon(request.icon);
			}, request.delay);
		} else {
			setIcon(request.icon)
		}


	}
});
